const express = require('express');
const http = require('http');
const path = require('path');
const cookieParser = require('cookie-parser');
const socketIOProvider = require('socket.io');
const cv = require('opencv4nodejs');

const fps = 30; //frames per second

 const port = process.env.PORT || 3000

 const indexRouter = require('./routes/index');
 
 const app = express();
 const server = http.Server(app);

/**
 * video source set to 0 for stream from webcam
 * video source can be set url from ip cam also eg: "http://192.168.1.112:8080/video"
 */
const videoSource = 0;
const io = socketIOProvider(server);
setInterval(() => {
    // const videoCap = new cv.VideoCapture(0);
    // const videoCap = new cv.VideoCapture("/dev/video0");
    const videoCap = new cv.VideoCapture("http://192.168.1.27:8080/shot.jpg");
    const frame = videoCap.read();
    const image = cv.imencode('.jpg', frame).toString('base64');
    io.emit('new-frame', { live: image });
}, 1000 / fps);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

server.listen(port, () => {
    console.log('Listening on', port);
});